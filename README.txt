E-ATM


The Backend of the app is written in .Net Core.

Please change the API Endpoint accordingly inside. src\environments\environment.ts


export const environment = {
  production: false,
  ApiEndPoint: 'http://localhost:5000/api/'
};

As this app just mocks the backend, all the accounts are hardcoded in the json files.

You can use one the following accounts.

[
  {
    "AccountId": "111111",
    "Pin": "1111",
    "Balance": 550.10,
    "Status": "Active",
    "Currency": "EUR - Euro",
    "CurrencySymbol":  "€"
  },
  {
    "AccountId": "222222",
    "Pin": "1234",
    "Balance": 1050.53,
    "Status": "Active",
    "Currency": "EUR - Euro",
    "CurrencySymbol": "€"
  },
  {
    "AccountId": "333333",
    "Pin": "1234",
    "Balance": 2000.00,
    "Status": "Active",
    "Currency": "EUR - Euro",
    "CurrencySymbol": "€"
  },
  {
    "AccountId": "444444",
    "Pin": "1234",
    "Balance": 5500.00,
    "Status": "Active",
    "Currency": "EUR - Euro",
    "CurrencySymbol": "€"
  },
  {
    "AccountId": "555555",
    "Pin": "5555",
    "Balance": 2350.00,
    "Status": "Active",
    "Currency": "EUR - Euro",
    "CurrencySymbol": "€"
  }
]