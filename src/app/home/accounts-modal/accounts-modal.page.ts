import { Component, OnInit } from '@angular/core';
import { AccountsService } from '../accounts.service';
import { ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { LoginAccount } from '../IAccount';


@Component({
  selector: 'app-accounts-modal',
  templateUrl: './accounts-modal.page.html',
  styleUrls: ['./accounts-modal.page.scss'],
})
export class AccountsModalPage implements OnInit {

  constructor(
              public accountsService: AccountsService,
              public modalController: ModalController,
              private router: Router) { }

  ngOnInit() {
  }

  SwitchUser(account: LoginAccount) {
    this.modalController.dismiss();
    this.accountsService.ActiveUser = account;
  }

  Logout() {
    const accountId = this.accountsService.ActiveUser.accountId;
    this.accountsService.LoggedInUsers = this.accountsService.LoggedInUsers.filter((ele) => {
      return ele.accountId != accountId;
    });

    this.NewUserLogin();
  }

  NewUserLogin(){
    this.modalController.dismiss();
    this.router.navigate(['login']);
  }
}
