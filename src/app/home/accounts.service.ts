import { Injectable } from '@angular/core';
import { LoginAccount } from './IAccount';
import { environment } from '../../environments/environment';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})

export class AccountsService {
  Accounts: LoginAccount[] = [];
  LoggedInUsers: LoginAccount[] = [];
  ActiveUser: LoginAccount = null;
  APIEndPoint = environment.ApiEndPoint;
  constructor(private http: HttpClient) { }

  Login(accountNo: number, pin: number, success: any, failed: any) {

    const headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json' );

    const postData = {  AccountId: accountNo, Pin: pin  };
    this.http.post<LoginAccount>(this.APIEndPoint + 'accounts/login', postData)
    .subscribe(data => success(data), (error: HttpErrorResponse) => failed(error));




    // const UserAccount =  this.Accounts.find(AccountInfo => {
    //   return accountNo == AccountInfo.AccountNo && pin == AccountInfo.Pin;
    // });
    // if (typeof UserAccount === 'undefined') {
    //   return false;
    // } else {
    //   this.ActiveUser = UserAccount;

    //   if (this.LoggedInUsers.indexOf(UserAccount) === -1) {
    //     this.LoggedInUsers.push(UserAccount);
    //   }

  }

  Withdraw(amount: number, success: any, failed: any){
    const headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json' );

    const postData = {  AccountId: this.ActiveUser.accountId, Pin: this.ActiveUser.pin, Amount: amount  };
    this.http.post<LoginAccount>(this.APIEndPoint + 'accounts/withdraw', postData)
    .subscribe(data => success(data) , (error: HttpErrorResponse) => failed(error));
  }


}
