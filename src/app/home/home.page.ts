import { Component } from '@angular/core';
import { AccountsService } from './accounts.service';
import { ModalController, AlertController } from '@ionic/angular';
import {FormGroup, FormBuilder, FormControl, Validators} from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import {AccountValidator} from './AccountValidator';
import { LoginAccount } from './IAccount';
import { Router } from '@angular/router';
import { AccountsModalPage } from './accounts-modal/accounts-modal.page';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage {
  Currencies = {'EUR - Euro': '€'};
  AccountForm: FormGroup;
  Error: string;
  constructor(private accountsService: AccountsService,
              private formBuilder: FormBuilder,
              public modalController: ModalController,
              private router: Router,
              public alertController: AlertController) {
    this.AccountForm = this.formBuilder.group({
      AccountNo: new FormControl('', [Validators.required, AccountValidator.Validate(1000, 999999)]),
      Pin: new FormControl('', Validators.compose([Validators.required, AccountValidator.Validate(1000, 9999)]))
    });
  }

  Login() {
      if (this.AccountForm.valid ) {
        this.accountsService.Login(
                                    this.AccountForm.value.AccountNo,
                                    this.AccountForm.value.Pin,
                                    (account: LoginAccount) => {
                                      this.accountsService.ActiveUser = account;
                                      this.accountsService.ActiveUser.currencySymbol = this.Currencies[
                                                                                       this.accountsService.ActiveUser.currency
                                                                                       ];
                                      if (!this.accountsService.LoggedInUsers.find(a => a.accountId == account.accountId)) {
                                        this.accountsService.LoggedInUsers.push(this.accountsService.ActiveUser);
                                      }
                                      this.router.navigate(['dashboard']);
                                    },
                                    async (error: HttpErrorResponse) => {
                                      this.Error = error.error.message;
                                      const alert = await this.alertController.create({
                                        header: 'Login Failed',
                                        message: this.Error,
                                        buttons: ['OK']
                                      });
                                      await alert.present();
                                    });
      }
  }

  async ShowLoggedInUsers() {
    const modal = await this.modalController.create({
    component: AccountsModalPage,
    cssClass: 'my-custom-modal-css'
    });
    return await modal.present();
  }

}
