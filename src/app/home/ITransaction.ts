
interface Transaction {
    amount: number;
    balance: number;
}

export interface TransactionResponse{
    action: boolean;
    message: string;
    transaction: Transaction;
}
