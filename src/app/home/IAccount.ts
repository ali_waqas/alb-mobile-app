
export interface LoginAccount{
    accountId: string;
    pin: string;
    balance: number;
    currency: string;
    currencySymbol: string;
    sessionId: string;
    attemptsFailed: number;
    status: string;
}