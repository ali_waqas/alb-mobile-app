import { Component, OnInit } from '@angular/core';
import { AccountsService } from '../home/accounts.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ModalController, AlertController } from '@ionic/angular';
import { AccountValidator } from '../home/AccountValidator';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { TransactionResponse } from '../home/ITransaction';
import { AccountsModalPage } from '../home/accounts-modal/accounts-modal.page';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})

export class DashboardPage {
  WithdrawForm: FormGroup;
  Error: string;
  ShowWithdrawForm = false;
  constructor(public accounsService: AccountsService,
              private formBuilder: FormBuilder,
              public alertController: AlertController,
              public modalController: ModalController,
              private router: Router) {
    this.WithdrawForm = this.formBuilder.group({Amount: new FormControl('', [Validators.required, AccountValidator.Validate(10, 1000)]) });
    this.CheckLogin();
  }

  CheckLogin() {
    if (this.accounsService.ActiveUser == null){
      this.router.navigate(['login']);
    }
  }

  ToggleWithdrawForm() {
    this.ShowWithdrawForm = !this.ShowWithdrawForm;
  }

  Withdraw() {
    if (this.WithdrawForm.valid) {
      this.accounsService.Withdraw(this.WithdrawForm.value.Amount,
      async (transactionResponse: TransactionResponse) => {
        const alert = await this.alertController.create({
          header: 'Withdrawal Successful',
          message: transactionResponse.message,
          buttons: ['OK']
        });
        await alert.present();
        this.accounsService.ActiveUser.balance = transactionResponse.transaction.balance;
        this.WithdrawForm.value.Amount = null;
        this.ToggleWithdrawForm();
      }, async (error: HttpErrorResponse) => {
        this.Error = error.error.message;
        const alert = await this.alertController.create({
          header: 'Withdrawal Failed',
          message: this.Error,
          buttons: ['OK']
        });
        await alert.present();
      });
    }
  }

  async ShowLoggedInUsers() {
    const modal = await this.modalController.create({
    component: AccountsModalPage,
    cssClass: 'my-custom-modal-css'
    });
    return await modal.present();
  }
}
